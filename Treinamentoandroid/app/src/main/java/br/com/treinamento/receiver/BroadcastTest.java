package br.com.treinamento.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import br.com.treinamento.services.ServicoTest;

/**
 * Created by charlie.silva on 08/11/2016.
 */

public class BroadcastTest extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("TESTE", "onReceive()");
        intent = new Intent(context, ServicoTest.class);
        context.startService(intent);
    }
}
