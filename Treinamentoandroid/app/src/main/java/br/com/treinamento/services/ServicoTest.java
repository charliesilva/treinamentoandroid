package br.com.treinamento.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlie.silva on 07/11/2016.
 */

public class ServicoTest extends Service {

    List<Worker> works = new ArrayList<>();

    @Override
    public IBinder onBind(Intent intent) {


        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("TESTE", "onStartCommand()");

        Worker w = new Worker(startId);
        w.start();
        works.add(w);

        return START_NOT_STICKY;

//        return super.onStartCommand(intent, flags, startId);
    }

    class Worker extends Thread {
        public int count = 0;
        public int startId;
        public boolean ativo = true;

        public Worker(int startId) {
            this.startId = startId;
        }

        @Override
        public void run() {
            while (ativo && count <= 1000) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count++;
                Log.i("TESTE", "count:" + count);
            }
            stopSelf();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        for (Worker w : works) {
            w.ativo = false;
        }

    }
}
