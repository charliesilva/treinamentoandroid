package br.com.treinamento.entity;

import java.io.Serializable;

/**
 * Created by charlie.silva on 20/10/2016.
 */

public class Pessoa implements Serializable {
    private Integer id;
    private String nome;
    private String idade;
    private String cidade;

    public Pessoa(String nome, String idade, String cidade) {
        this.nome = nome;
        this.idade = idade;
        this.cidade = cidade;
    }

    public Pessoa() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @Override
    public String toString() {
        return this.nome + " - " + this.cidade;
    }
}
