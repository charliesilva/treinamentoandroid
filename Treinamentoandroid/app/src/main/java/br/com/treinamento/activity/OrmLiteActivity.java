package br.com.treinamento.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.R;
import br.com.treinamento.adapter.PessoaAdapterOrmLite;
import br.com.treinamento.entity.PessoaOrmLite;
import br.com.treinamento.util.DatabaseHelper;

public class OrmLiteActivity extends AppCompatActivity {

    private ListView listViewOrmLite;
    private Button btnCadastrarOrm;
    private PessoaAdapterOrmLite adapter;
    private DatabaseHelper databaseHelper;
    private List<PessoaOrmLite> pessoas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orm_lite);
        listViewOrmLite = (ListView) findViewById(R.id.listViewOrmLite);
        btnCadastrarOrm = (Button) findViewById(R.id.btnCadastrarOrm);

        databaseHelper = new DatabaseHelper(this).getHelper(this);

        btnCadastrarOrm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrmLiteActivity.this,
                        CadastroOrmLiteActivity.class);
                CadastroOrmLiteActivity.pessoa = null;
                startActivity(intent);
            }
        });

               listViewOrmLite.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                abreAlteracaoExclusao((PessoaOrmLite) adapterView.
                        getItemAtPosition(position));
            }
        });

        listView();
    }

    private void listView() {
        try {
            pessoas = databaseHelper.
                    getPessoaOrmLiteDAO(this).queryForAll();
            adapter = new PessoaAdapterOrmLite(this,
                    (ArrayList<PessoaOrmLite>) pessoas);
            listViewOrmLite.setAdapter(adapter);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void abreAlteracaoExclusao(PessoaOrmLite pessoa) {
        CadastroOrmLiteActivity.pessoa = pessoa;
        startActivity(new Intent(this, CadastroOrmLiteActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
