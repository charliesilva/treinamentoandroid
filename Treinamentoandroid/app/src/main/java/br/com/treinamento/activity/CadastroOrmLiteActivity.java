package br.com.treinamento.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.SQLException;

import br.com.treinamento.R;
import br.com.treinamento.entity.PessoaOrmLite;
import br.com.treinamento.util.DatabaseHelper;

public class CadastroOrmLiteActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;
    public static PessoaOrmLite pessoa;
    private EditText edtNome;
    private EditText edtIdade;
    private EditText edtCidade;
    private Button btnExcluir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_orm_lite);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtIdade = (EditText) findViewById(R.id.edtIdade);
        edtCidade = (EditText) findViewById(R.id.edtCidade);
        btnExcluir = (Button) findViewById(R.id.btnExcluir);

        databaseHelper = new DatabaseHelper(this).getHelper(this);

        if (pessoa != null && pessoa.getId() != null) {
            btnExcluir.setVisibility(View.VISIBLE);
            edtNome.setText(pessoa.getNome());
            edtIdade.setText(pessoa.getIdade());
            edtCidade.setText(pessoa.getCidade());
        } else {
            btnExcluir.setVisibility(View.GONE);
        }

        btnExcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    databaseHelper.getPessoaOrmLiteDAO(CadastroOrmLiteActivity.this).delete(pessoa);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                finish();
            }
        });


    }

    public void salvar(View view) {
        try {
            pessoa = (pessoa == null ? new PessoaOrmLite() : pessoa);
            pessoa.setNome(edtNome.getText().toString());
            pessoa.setIdade(edtIdade.getText().toString());
            pessoa.setCidade(edtCidade.getText().toString());

            if (pessoa.getId() != null) {
                databaseHelper.getPessoaOrmLiteDAO(this).update(pessoa);
            } else {
                databaseHelper.getPessoaOrmLiteDAO(this).create(pessoa);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finish();
    }

}
