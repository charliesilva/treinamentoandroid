package br.com.treinamento.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.treinamento.R;

public class DownloadImageActivity extends AppCompatActivity {

    //http://www.el.com.br/wp-content/uploads/2015/03/EL_Logomarca-branca.png

    private ImageView imagem;
    private Handler handler = new Handler();
    private static Bitmap imgAux;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_image);
        imagem = (ImageView) findViewById(R.id.imagem);

        if (imgAux != null) {
            imagem.setImageBitmap(imgAux);
        }

    }

    public void loadImg(View view) {
        progress = ProgressDialog.show(this, "Aguarde", "Carregando imagem", false, false);
        new Thread() {
            public void run() {
                Bitmap img = null;
                try {
                    URL url = new URL("http://www.el.com.br/wp-content/uploads/2015/03/EL_Logomarca-branca.png");
                    HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
                    InputStream input = conexao.getInputStream();
                    img = BitmapFactory.decodeStream(input);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                imgAux = img;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        imagem.setImageBitmap(imgAux);
                        progress.dismiss();
                    }
                });
            }
        }.start();


    }


}
