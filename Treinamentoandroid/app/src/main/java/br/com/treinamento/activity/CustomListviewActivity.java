package br.com.treinamento.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.adapter.PessoaAdapter;
import br.com.treinamento.R;
import br.com.treinamento.entity.Pessoa;

public class CustomListviewActivity extends AppCompatActivity {

    private PessoaAdapter adapter;
    private List<Pessoa> pessoas = new ArrayList<>();
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_listview);
        Pessoa pessoa1 = new Pessoa("Charlie", "28 anos", "Domingos Martins");
        pessoas.add(pessoa1);
        Pessoa pessoa2 = new Pessoa("João", "28 anos", "Domingos Martins");
        pessoas.add(pessoa2);
        Pessoa pessoa3 = new Pessoa("Fernando", "28 anos", "Domingos Martins");
        pessoas.add(pessoa3);
        Pessoa pessoa4 = new Pessoa("Fabiana", "28 anos", "Domingos Martins");
        pessoas.add(pessoa4);


        listView = (ListView) findViewById(R.id.listViewPessoa);

        adapter = new PessoaAdapter(CustomListviewActivity.this, (ArrayList<Pessoa>) pessoas);

        listView.setAdapter(adapter);

    }
}
