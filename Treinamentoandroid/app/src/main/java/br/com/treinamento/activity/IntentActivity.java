package br.com.treinamento.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import br.com.treinamento.R;
import br.com.treinamento.entity.Pessoa;

public class IntentActivity extends AppCompatActivity {

    private EditText edtUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        edtUrl = (EditText) findViewById(R.id.edtUrl);

        //ACTION_CALL
        //ACTION_EDIT
        //ACTION_SENDTO


    }

    public void abrirUrl(View view) {
        Uri uri = Uri.parse("http://" + edtUrl.getText().toString());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void enviarString(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("URL", edtUrl.getText().toString());
        Intent intent = new Intent(this, IntentAuxActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void passarObjeto(View view) {
        Intent intent = new Intent(this, IntentAuxActivity.class);
        intent.putExtra("PESSOA", new Pessoa("Charlie", "29", "DM"));
        startActivity(intent);
    }


}
