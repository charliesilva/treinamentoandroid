package br.com.treinamento.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.entity.Pessoa;
import br.com.treinamento.util.DBHelper;


public class PessoaDAO {
    private SQLiteDatabase db;
    private String[] colunas =
            {
                    DBHelper.COLUNA_ID,
                    DBHelper.COLUNA_NOME,
                    DBHelper.COLUNA_IDADE,
                    DBHelper.COLUNA_CIDADE
            };

    private DBHelper helper;

    public PessoaDAO(Context context) {
        helper = new DBHelper(context);
    }

    public void abrir() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void fechar() {
        helper.close();
    }

    public Pessoa inserir(Pessoa pessoa) {

        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUNA_NOME, pessoa.getNome());
        values.put(DBHelper.COLUNA_IDADE, pessoa.getIdade());
        values.put(DBHelper.COLUNA_CIDADE, pessoa.getCidade());


        long insertId = db.insert(DBHelper.NOME_TABELA, null, values);

        Cursor cursor = db.query(DBHelper.NOME_TABELA, colunas,
                DBHelper.COLUNA_ID + " = " + insertId,
                null, null, null, null);

        cursor.moveToFirst();
        cursor.close();
        return pessoa;
    }

    public void excluir(Pessoa pessoa) {
        long id = pessoa.getId();
        db.delete(DBHelper.NOME_TABELA, DBHelper.COLUNA_ID
                + " = " + id, null);
    }

    public List<Pessoa> todas() {
        List<Pessoa> pessoas = new ArrayList<Pessoa>();

        Cursor cursor = db.query(DBHelper.NOME_TABELA, colunas,
                null, null, null, null, DBHelper.COLUNA_NOME);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Pessoa pessoa = new Pessoa();
            pessoa.setId(cursor.getInt(0));
            pessoa.setNome(cursor.getString(1));
            pessoa.setIdade(cursor.getString(2));
            pessoa.setCidade(cursor.getString(3));
            pessoas.add(pessoa);
            cursor.moveToNext();
        }
        cursor.close();
        return pessoas;
    }
}
