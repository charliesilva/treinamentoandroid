package br.com.treinamento.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.treinamento.dao.PessoaOrmLiteDAO;
import br.com.treinamento.entity.PessoaOrmLite;

/**
 * Created by charlie.silva on 01/11/2016.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "PessoaOrmLite.db";
    private static final int DATABASE_VERSION = 2;
    private Context context;
    private PessoaOrmLiteDAO pessoaOrmLiteDAO;
    private static DatabaseHelper databaseHelper;
    private static final AtomicInteger usageCounter = new AtomicInteger(0);
    private final String TAG = DatabaseHelper.class.getSimpleName();


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {

            TableUtils.createTable(connectionSource, PessoaOrmLite.class);

        } catch (java.sql.SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int i, int i1) {
        try {

            TableUtils.dropTable(connectionSource, PessoaOrmLite.class, false);
            onCreate(db, connectionSource);

        } catch (Exception e) {
            Log.e(TAG, "Erro ao criar banco ", e);
        }
    }

    public static synchronized DatabaseHelper getHelper(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        usageCounter.incrementAndGet();
        return databaseHelper;
    }

    public PessoaOrmLiteDAO getPessoaOrmLiteDAO(Context context) throws java.sql.SQLException {
        try {
            if (pessoaOrmLiteDAO == null) {
                pessoaOrmLiteDAO = new PessoaOrmLiteDAO(context, connectionSource, PessoaOrmLite.class);
            }
        } catch (SQLException ex) {
            Log.w(DatabaseHelper.class.getName(), "Erro ao criar pessoaOrmLiteDAO ", ex);
        }
        return pessoaOrmLiteDAO;
    }

}
