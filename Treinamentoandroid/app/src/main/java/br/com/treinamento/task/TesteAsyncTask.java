package br.com.treinamento.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.treinamento.interfaces.Tarefas;

/**
 * Created by charlie.silva on 09/11/2016.
 */

public class TesteAsyncTask extends AsyncTask<String, String, Bitmap> {

    private ProgressDialog progress;
    private Context context;
    private Tarefas tarefas;

    public TesteAsyncTask(Context context, Tarefas tarefas) {
        this.context = context;
        this.tarefas = tarefas;
    }

    @Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(context, "Info", "Baixando imagem...", false, false);
    }

    @Override
    protected Bitmap doInBackground(String... pUrl) {
        Bitmap img = null;
        try {
            URL url = new URL(pUrl[0]);
            HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
            InputStream input = conexao.getInputStream();
            img = BitmapFactory.decodeStream(input);

            if (img != null) {
                publishProgress("Imagem baixada com sucesso!");
            } else {
                publishProgress("Erro ao baixar imagem!");
            }

            Thread.sleep(4 * 1000);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return img;
    }

    @Override
    protected void onProgressUpdate(String... parans) {
        progress.setMessage(parans[0]);
    }

    @Override
    protected void onPostExecute(Bitmap img) {
        progress.dismiss();
        tarefas.despoisDeBaixarImagem(img);
    }
}
