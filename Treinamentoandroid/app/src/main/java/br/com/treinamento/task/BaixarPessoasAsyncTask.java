package br.com.treinamento.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.treinamento.entity.PessoaOrmLite;
import br.com.treinamento.interfaces.Tarefas;
import br.com.treinamento.util.WebClient;

/**
 * Created by charlie.silva on 09/11/2016.
 */

public class BaixarPessoasAsyncTask extends AsyncTask<String, String, List<PessoaOrmLite>> {

    private ProgressDialog progress;
    private Context context;
    private Tarefas tarefas;

    public BaixarPessoasAsyncTask(Context context, Tarefas tarefas) {
        this.context = context;
        this.tarefas = tarefas;
    }

    @Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(context, "Info", "Baixando Pessoas...", false, false);
    }

    @Override
    protected List<PessoaOrmLite> doInBackground(String... pUrl) {
        List<PessoaOrmLite> pessoas = new ArrayList<>();

        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return  new Date((json.getAsJsonPrimitive().getAsLong()));
            }
        });

        Gson gson = builder.setDateFormat("yyyy-MM-dd").create();

        String webclient = new WebClient(pUrl[0],context).get("");

        pessoas = gson.fromJson(webclient, new TypeToken<List<PessoaOrmLite>>() {}.getType());

        return pessoas;
    }

    @Override
    protected void onProgressUpdate(String... parans) {
        progress.setMessage(parans[0]);
    }

    @Override
    protected void onPostExecute(List<PessoaOrmLite> pessoas) {
        progress.dismiss();
        tarefas.despoisDeBaixarPessoas(pessoas);
    }

    private  String porcentagem(double atual, double end) {
        double retorno;
        retorno = ((atual / end) * 100.0);

        return String.format("%.0f", retorno);
    }
}
