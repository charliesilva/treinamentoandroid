package br.com.treinamento.activity;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.treinamento.R;
import br.com.treinamento.entity.Pessoa;

public class IntentAuxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entity_aux);
    }

    public void recuperarString(View view) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String string = bundle.getString("URL");
            AlertDialog.Builder msg = new AlertDialog.Builder(IntentAuxActivity.this);
            msg.setTitle("String")
                    .setMessage(string)
                    .setPositiveButton("OK", null)
                    .show();
        }
    }

    public void recuperarObjeto(View view) {
        Pessoa pessoa = (Pessoa) getIntent().getSerializableExtra("PESSOA");

        if (pessoa != null) {
            AlertDialog.Builder msg = new AlertDialog.Builder(IntentAuxActivity.this);
            msg.setTitle("Objeto")
                    .setMessage(pessoa.getNome())
                    .setPositiveButton("OK", null)
                    .show();
        }
    }
}
