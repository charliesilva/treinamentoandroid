package br.com.treinamento.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.R;
import br.com.treinamento.adapter.PessoaAdapterOrmLite;
import br.com.treinamento.entity.PessoaOrmLite;
import br.com.treinamento.interfaces.Tarefas;
import br.com.treinamento.task.BaixarPessoasAsyncTask;

public class JsonActivity extends AppCompatActivity implements Tarefas {

    private ListView listViewPessoas;
    private PessoaAdapterOrmLite adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);
        listViewPessoas = (ListView) findViewById(R.id.listViewPessoasJ);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pessoas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.baixarPessoas:
                BaixarPessoasAsyncTask baixarPessoasAsyncTask =
                        new BaixarPessoasAsyncTask(JsonActivity.this, JsonActivity.this);
                baixarPessoasAsyncTask.execute("http://beta.json-generator.com/api/json/get/VyfwLLV-G");
                break;
        }
        return true;
    }

    @Override
    public void despoisDeBaixarImagem(Bitmap img) {

    }

    @Override
    public void despoisDeBaixarPessoas(List<PessoaOrmLite> pessoas) {
        adapter = new PessoaAdapterOrmLite(this, (ArrayList<PessoaOrmLite>) pessoas);
        listViewPessoas.setAdapter(adapter);
    }
}
