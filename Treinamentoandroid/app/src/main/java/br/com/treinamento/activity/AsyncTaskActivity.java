package br.com.treinamento.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import br.com.treinamento.R;
import br.com.treinamento.entity.PessoaOrmLite;
import br.com.treinamento.interfaces.Tarefas;
import br.com.treinamento.task.TesteAsyncTask;

public class AsyncTaskActivity extends AppCompatActivity implements Tarefas {

    private ImageView imagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);

        imagem = (ImageView) findViewById(R.id.imagem);
    }

    public void loadImg(View view) {
        TesteAsyncTask testeAsyncTask = new TesteAsyncTask(AsyncTaskActivity.this, AsyncTaskActivity.this);
        testeAsyncTask.execute("http://www.el.com.br/wp-content/uploads/2015/03/EL_Logomarca-branca.png");

    }

    @Override
    public void despoisDeBaixarImagem(Bitmap img) {
        imagem.setImageBitmap(img);
    }

    @Override
    public void despoisDeBaixarPessoas(List<PessoaOrmLite> pessoas) {

    }


}
