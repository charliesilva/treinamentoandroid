package br.com.treinamento.dao;

import android.content.Context;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.entity.PessoaOrmLite;
import br.com.treinamento.util.DatabaseHelper;

/**
 * Created by charlie.silva on 01/11/2016.
 */

public class PessoaOrmLiteDAO extends BaseDaoImpl<PessoaOrmLite, Integer> {

    private DatabaseHelper databaseHelper;
    private Context context;

    public PessoaOrmLiteDAO(Context context, ConnectionSource connectionSource, Class<PessoaOrmLite> dataClass) throws SQLException, java.sql.SQLException {
        super(connectionSource, dataClass);
        this.context = context;

    }

    public List<PessoaOrmLite> getPessoas(String nome){
        try{
            databaseHelper = new DatabaseHelper(context).getHelper(context);
            List<PessoaOrmLite> pessoaOrmLites = new ArrayList<>();

            QueryBuilder qb = databaseHelper.getPessoaOrmLiteDAO(context).queryBuilder();

            pessoaOrmLites = qb.where().eq("nome",nome).query();

            return pessoaOrmLites;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }

}

