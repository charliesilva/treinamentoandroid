package br.com.treinamento.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.treinamento.R;
import br.com.treinamento.services.ServicoTest;

public class ServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

    }

    public void startServices(View view) {
        Intent intent = new Intent(getBaseContext(), ServicoTest.class);
        startService(intent);
    }

    public void stopServices(View view) {
        Intent intent = new Intent(getBaseContext(), ServicoTest.class);
        stopService(intent);
    }

}
