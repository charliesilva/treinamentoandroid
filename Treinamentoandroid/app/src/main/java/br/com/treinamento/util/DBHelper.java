package br.com.treinamento.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String NOME_DO_BANCO = "pessoas.db";
    public static final String NOME_TABELA = "pessoas";
    public static final String COLUNA_ID = "id";
    public static final String COLUNA_NOME = "nome";
    public static final String COLUNA_IDADE = "idade";
    public static final String COLUNA_CIDADE = "cidade";

    private static int VERSAO_DO_BANCO = 3;

    private static final String CREATE_TABLES =
            "create table " + NOME_TABELA +
                    " (" + COLUNA_ID + " integer primary key autoincrement, " +
                    COLUNA_NOME + " text not null," +
                    COLUNA_IDADE + " text," +
                    COLUNA_CIDADE + " text" +
                    ")";

    public DBHelper(Context context) {
        super(context, NOME_DO_BANCO, null, VERSAO_DO_BANCO);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + NOME_TABELA);
        onCreate(db);
    }

}