package br.com.treinamento.util;

import android.content.Context;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Gustavo Sperandio
 */
public class WebClient {

    private final String url;
    Context context;


    public WebClient(String url, Context context) {
        super();
        this.url = url;
        this.context = context;
    }

    public String post(String json) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            if (json != null) {
                post.setEntity(new StringEntity(json));
                post.setHeader("Content-type", "application/json");
            }

            post.setHeader("Content-type", "application/json; charset=ISO-8859-1");

            HttpResponse response = httpClient.execute(post);
            String jsonDeResposta = EntityUtils.toString(response.getEntity());


            return jsonDeResposta;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String get(String json) {
        try {
            InputStream inputStream = null;
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet get = new HttpGet(url);
            //get.setEntity(new StringEntity(json));

            get.setHeader("accept", "application/json");
            get.setHeader("Content-type", "application/json; charset=UTF-8");
            HttpResponse httpResponse = httpClient.execute(get);
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            String retorno = "";
            if (inputStream != null) {
                retorno = convertInputStreamToString(inputStream);
            }

            return retorno;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return null;
    }

    private String convertInputStreamToString(InputStream inputStream) throws UnsupportedEncodingException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

        String line = "";
        String result = "";

        try {
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }
        } catch (IOException ex) {
            Logger.getLogger(WebClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            inputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(WebClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
}
