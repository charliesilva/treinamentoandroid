package br.com.treinamento.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import br.com.treinamento.R;

public class CheckBoxActivity extends AppCompatActivity {

    private CheckBox checkBox;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_radio_button);

        checkBox = (CheckBox) findViewById(R.id.checkBox);
        imageView = (ImageView) findViewById(R.id.imageView);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (checkBox.isChecked()) {
                    imageView.setImageResource(R.drawable.ic_lightbulb_outline);
                    checkBox.setText("Luz acesa!");
                } else {
                    imageView.setImageResource(R.drawable.ic_lightbulb);
                    checkBox.setText("Luz apagada!");
                }

            }
        });

    }
}
