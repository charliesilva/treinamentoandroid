package br.com.treinamento.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import br.com.treinamento.R;

public class RadioButtonActivity extends AppCompatActivity {

    private RadioGroup groupResp;
    private RadioButton radioButton;
    private Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        groupResp = (RadioGroup) findViewById(R.id.groupResp);
        btnOk = (Button) findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                int selecao = groupResp.getCheckedRadioButtonId();

                radioButton = (RadioButton) findViewById(selecao);
                Toast toast = Toast.makeText(RadioButtonActivity.this,
                        radioButton.getText().toString(),
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        });



    }
}
