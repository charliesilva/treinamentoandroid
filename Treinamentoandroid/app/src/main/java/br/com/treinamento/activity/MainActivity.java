package br.com.treinamento.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import br.com.treinamento.R;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        String[] valores = new String[]{
                "Spinner", "ListView Customizado", "CheckBox", "Shared Preferences",
                "RadioButton", "Sqlite", "OrmLite", "Download de Imagem (Thread)", "ActionBar",
                "Download de Imagem (AsyncTask)", "Service", "Json (Gson)", "Intent"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_expandable_list_item_2,
                android.R.id.text1,
                valores);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long l) {

                String conteudo = (String) listView.getItemAtPosition(posicao);

                Toast toast = Toast.makeText(MainActivity.this, conteudo, Toast.LENGTH_SHORT);
                toast.show();
                Intent intent;

                switch (conteudo) {
                    case "Spinner":
                        intent = new Intent(MainActivity.this, SpinnerExemploActivity.class);
                        startActivity(intent);
                        break;
                    case "ListView Customizado":
                        intent = new Intent(MainActivity.this, CustomListviewActivity.class);
                        startActivity(intent);
                        break;
                    case "CheckBox":
                        intent = new Intent(MainActivity.this, CheckBoxActivity.class);
                        startActivity(intent);
                        break;
                    case "Shared Preferences":
                        intent = new Intent(MainActivity.this, SharedPreferenceActivity.class);
                        startActivity(intent);
                        break;
                    case "RadioButton":
                        intent = new Intent(MainActivity.this, RadioButtonActivity.class);
                        startActivity(intent);
                        break;
                    case "Sqlite":
                        intent = new Intent(MainActivity.this, SqliteActivity.class);
                        startActivity(intent);
                        break;
                    case "OrmLite":
                        intent = new Intent(MainActivity.this, OrmLiteActivity.class);
                        startActivity(intent);
                        break;
                    case "Download de Imagem (Thread)":
                        intent = new Intent(MainActivity.this, DownloadImageActivity.class);
                        startActivity(intent);
                        break;
                    case "ActionBar":
                        intent = new Intent(MainActivity.this, ActionBarActivity.class);
                        startActivity(intent);
                        break;
                    case "Download de Imagem (AsyncTask)":
                        intent = new Intent(MainActivity.this, AsyncTaskActivity.class);
                        startActivity(intent);
                        break;
                    case "Service":
                        intent = new Intent(MainActivity.this, ServiceActivity.class);
                        startActivity(intent);
                        break;
                    case "Json (Gson)":
                        intent = new Intent(MainActivity.this, JsonActivity.class);
                        startActivity(intent);
                        break;
                    case "Intent":
                        intent = new Intent(MainActivity.this, IntentActivity.class);
                        startActivity(intent);
                        break;
                }

            }
        });


    }
}
