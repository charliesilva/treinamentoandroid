package br.com.treinamento.interfaces;

import android.graphics.Bitmap;

import java.util.List;

import br.com.treinamento.entity.PessoaOrmLite;

/**
 * Created by charlie.silva on 09/11/2016.
 */

public interface Tarefas {
    public void despoisDeBaixarImagem(Bitmap img);
    public void despoisDeBaixarPessoas(List<PessoaOrmLite> pessoas);
}
