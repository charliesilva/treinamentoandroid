package br.com.treinamento.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.treinamento.R;
import br.com.treinamento.dao.PessoaDAO;
import br.com.treinamento.entity.Pessoa;

public class CadastroActivity extends AppCompatActivity {

    private PessoaDAO dao;
    public static Pessoa pessoa;
    private EditText edtNome;
    private EditText edtIdade;
    private EditText edtCidade;
    private Button btnExcluir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        edtNome = (EditText) findViewById(R.id.edtNome);
        edtIdade = (EditText) findViewById(R.id.edtIdade);
        edtCidade = (EditText) findViewById(R.id.edtCidade);
        btnExcluir = (Button) findViewById(R.id.btnExcluir);

        dao = new PessoaDAO(this);
        dao.abrir();

        if(pessoa != null && pessoa.getId() !=  null){
            edtNome.setText(pessoa.getNome());
            edtIdade.setText(pessoa.getIdade());
            edtCidade.setText(pessoa.getCidade());

           btnExcluir.setVisibility(View.VISIBLE);
        }

    }

    public void excluir(View v) {
        dao.excluir(pessoa);
        finish();
    }

    public void salvar(View v) {
        pessoa = (pessoa == null ? new Pessoa() : pessoa);
        pessoa.setNome(edtNome.getText().toString());
        pessoa.setIdade(edtIdade.getText().toString());
        pessoa.setCidade(edtCidade.getText().toString());
        dao.inserir(pessoa);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dao.fechar();
    }

}
