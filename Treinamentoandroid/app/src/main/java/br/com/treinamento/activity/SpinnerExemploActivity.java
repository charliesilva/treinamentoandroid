package br.com.treinamento.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.R;
import br.com.treinamento.entity.Pessoa;

public class SpinnerExemploActivity extends AppCompatActivity {

    private Spinner spinner;

    private TextView txvSelecionado;
    private Spinner spnPessoa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_exemplo);
        spinner = (Spinner) findViewById(R.id.spinner);

        txvSelecionado = (TextView) findViewById(R.id.txvSelecionado);
        spnPessoa = (Spinner) findViewById(R.id.spnPessoa);

        String[] valores = new String[]{
                "item1", "item2", "item3", "item4", "item5", "item6"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SpinnerExemploActivity.this,
                android.R.layout.simple_expandable_list_item_2,
                android.R.id.text1,
                valores);

        spinner.setAdapter(adapter);
        /////////Objetos

        List<Pessoa> pessoas = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Charlie", "28 anos", "Domingos Martins");
        pessoas.add(pessoa1);
        Pessoa pessoa2 = new Pessoa("João", "28 anos", "Domingos Martins");
        pessoas.add(pessoa2);
        Pessoa pessoa3 = new Pessoa("Fernando", "28 anos", "Domingos Martins");
        pessoas.add(pessoa3);
        Pessoa pessoa4 = new Pessoa("Fabiana", "28 anos", "Domingos Martins");
        pessoas.add(pessoa4);

        ArrayAdapter pessoaAdapter =

                new ArrayAdapter(SpinnerExemploActivity.this,
                android.R.layout.simple_list_item_2,
                android.R.id.text2,
                pessoas);

        spnPessoa.setAdapter(pessoaAdapter);

        spnPessoa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {

                Pessoa p = (Pessoa) parent.getSelectedItem();

                txvSelecionado.setText("Selecionado: " + p.getNome() + " - " + p.getCidade());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }
}
