package br.com.treinamento.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.treinamento.R;
import br.com.treinamento.adapter.PessoaAdapterSqlite;
import br.com.treinamento.dao.PessoaDAO;
import br.com.treinamento.entity.Pessoa;

public class SqliteActivity extends AppCompatActivity {

    private ListView listViewSqlite;
    private Button button;
    private PessoaAdapterSqlite adapter;
    private PessoaDAO dao;
    private List<Pessoa> pessoas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);


        listViewSqlite = (ListView) findViewById(R.id.listViewSqlite);
        button = (Button) findViewById(R.id.button);
        dao = new PessoaDAO(this);
        pessoas = new ArrayList<Pessoa>();

        listViewSqlite.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                abreAlteracaoExclusao((Pessoa) adapterView.getItemAtPosition(position));
            }
        });
        dao.abrir();
        listView();

    }

    public void abreAlteracaoExclusao(Pessoa pessoa) {
        CadastroActivity.pessoa = pessoa;
        startActivity(new Intent(SqliteActivity.this, CadastroActivity.class));
    }

    public void listView() {
        try {
            pessoas = dao.todas();
            adapter = new PessoaAdapterSqlite(this, (ArrayList<Pessoa>) pessoas);
            listViewSqlite.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cadastrar(View view) {
        Intent intent = new Intent(this, CadastroActivity.class);
        CadastroActivity.pessoa = null;
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listView();
    }
}

