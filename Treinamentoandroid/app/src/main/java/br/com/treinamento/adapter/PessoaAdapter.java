package br.com.treinamento.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.treinamento.R;
import br.com.treinamento.entity.Pessoa;

/**
 * Created by charlie.silva on 20/10/2016.
 */
public class PessoaAdapter extends ArrayAdapter<Pessoa> {

    public PessoaAdapter(Context context, ArrayList<Pessoa> pessoas) {
        super(context, 0, pessoas);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Pessoa p = (Pessoa) getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_listview,
                    parent, false);
        }

        TextView txvLinha1 = (TextView) convertView.findViewById(R.id.txvLinha1);
        TextView txvLinha2 = (TextView) convertView.findViewById(R.id.txvLinha2);

        txvLinha1.setText(p.getNome());
        txvLinha2.setText(p.getCidade());

        return convertView;
    }
}
