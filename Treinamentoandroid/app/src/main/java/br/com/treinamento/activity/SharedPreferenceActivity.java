package br.com.treinamento.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import br.com.treinamento.R;

public class SharedPreferenceActivity extends AppCompatActivity {

    private EditText edtEmail;
    private EditText edtSenha;
    private Button bntAcessar;
    private CheckBox chkSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        bntAcessar = (Button) findViewById(R.id.bntAcessar);
        chkSave = (CheckBox) findViewById(R.id.chkSave);

        final SharedPreferences preferences = getApplicationContext().getSharedPreferences("preferencia", MODE_PRIVATE);

        edtEmail.setText(preferences.getString("email", ""));
        edtSenha.setText(preferences.getString("senha", ""));
        chkSave.setChecked(preferences.getBoolean("save", false));

        chkSave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences.Editor editor = preferences.edit();
                if (chkSave.isChecked()) {
                    editor.putString("email", edtEmail.getText().toString());
                    editor.putString("senha", edtSenha.getText().toString());
                    editor.putBoolean("save", chkSave.isChecked());
                    editor.commit();
                } else {
                    editor.putString("email", "");
                    editor.putString("senha", "");
                    editor.putBoolean("save", false);
                    editor.commit();
                }
            }
        });

        bntAcessar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtEmail.getText().toString().equals("charlie.honorato.ch@gmail.com") &&
                        edtSenha.getText().toString().equals("123")) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(SharedPreferenceActivity.this);
                    msg.setTitle("Info");
                    msg.setMessage("Correto");
                    msg.setPositiveButton("OK", null);
                    msg.show();
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(SharedPreferenceActivity.this);
                    msg.setTitle("Info");
                    msg.setMessage("Errado");
                    msg.setPositiveButton("OK", null);
                    msg.show();
                }
            }
        });


    }
}
